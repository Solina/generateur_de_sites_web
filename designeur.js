
"use strict";

var objetSelectione;

// Ce lien est dynamique, il faut utiliser une ID
var croix_lien = "img/croix.jpg";

var etagere, espace_cliquable, conteneurGuache, espace, panneauDroit;
var titre;
var etagereTip = []
var scene = {
"name":"",
"listeImage":[],
"listeEtagere":[]

};

var listeEtagere=[
      {
        "imageEtagere": {},
        "listeCroixEtagere": []
      }]
var maScene = [];
var mesEtageres=[];
var xx, yy, urlSaisi;
var tip;
var mesObjet;
var id = 0;
var idUrl = 0;
var idLien = 0;
var idCroix = 0;
var idSvgEtagere = 0;
var titreSaisi;
var imageoui = false;
var heightScene = 600;
var widthScene = 600;
var clickCroix = false;
var menu = [
  {
    title: 'height',
    idRange: 'rangeH',
    action: function () { return; }
  },
  {
    title: 'width',
    idRange: 'rangeW',

    action: function () { return; }
  }
];


function init() {
  var conteneurSeine = d3.selectAll('#scene')
    .attr("class", "scene")
    .attr("height", heightScene)
    .attr("width", widthScene)
    .attr("preserveAspectRatio", "none"); // c'est pour préserver la forme de l'image 

  conteneurSeine.on('contextmenu', function () {
    if (!imageoui) {
      return d3.contextMenu(menu, conteneurSeine, imageoui);

    } else { d3.event.preventDefault(); imageoui = false; }
  });
  //ajouter un titre
  d3.selectAll('#scene').html('');

  d3.selectAll('#scene').append("div")
    .attr("id", "divTitre");
  titre = d3.selectAll("#divTitre")
    .append('h')
    .attr("id", "titreScene")
  //.text("mon titre de scene :");
  d3.selectAll('#divTitre').append("div")
    .attr("id", "divInputTitre");
  var label = d3.selectAll("#divInputTitre").append("label")
    .attr("id", "labelInputTitre")
    .attr("for", "inputTitre")
    .text("label : ");
  var input = d3.selectAll("#divInputTitre").append('input')
    .attr("id", "inputTitre")
    .attr("type", "text")
    .attr("name", "inputTitre")
    .on("input", function () {
      console.log("le titre saisi: " + this.value)
      titreSaisi = this.value;

      //apres cette la copie dans url: lien (en haut de input) sous form de lien clickable 

      d3.select("#titreScene").text(titreSaisi);
      scene.name = titreSaisi;
    });
  // .html(function(d) {return "mon titre de scene :";});
  //saut de ligne :
  // d3.selectAll('#scene').append("br")
  // La zone mère à gauche, qui a un contour noir
  conteneurGuache = d3.select("#scene").append("svg")
    .attr("id", "svg_container")
    .attr("style", "border:2px solid black")
   .attr("width", 510). 
   attr("height", 505);
  // conteneurGuache.on('contextmenu', d3.contextMenu(menu,conteneurGuache));
  ;


  // L'espace utilisée pour le dessin
  // Il s'agit d'un autre svg dans la zone mère

  espace = conteneurGuache.append("svg").
    attr("id", "object_space")
   .attr("width", 510). 
   attr("height",505); 

  // Le panneau à droite qui contient les objets/images 
  panneauDroit = d3.select("#scene").append("svg").
    attr("id", "object_panel")
   .attr("width", 70). 
   attr("height", 505); 




  //2 ajouter un rectangle où on va dessiner nos images

  // L'objet SVG est vide par défaut, et lorsqu'on clique sur le vide
  // l'évenement "click" ne fera rien, et la fonction dessinerNewObject
  // ne sera pas exécutée.
  // Donc il faut aujouter un rectangle qui va recevoir les cliques
  // et exécuter la fonction dessinerNewObject.

  espace_cliquable = espace.append("rect")
    .attr("id", "idRect")
    .attr("x", "0")
    .attr("y", "0")
     .attr("width",510)
     .attr("height",505)
    .attr("fill-opacity", "0.0") // transparent
    .on("click", dessinerNewObject);
  /*	   
    etagere = espace.append("svg:image")
          .attr("id","etagere")
          .attr("class","mesImage")
         .attr("xlink:href", "./img/etagere.jpg") 
          .attr("x","0")
          .attr("y","0")
         .attr("width","250")
         .attr("height","250")
         //le click sur l etagère va déclancher l' ajout de tip(infobull)
       .on("click", ajoutertip); 
  */




  //3 charger les data d'un fichier json

  $.getJSON("img/img.json", function (data) {
    var n = 0;
     var distance = 0
    // chemin 
    $.each(data, function (i, field) {
      console.log("field.lien: " + field.lien);


      //récuperer les champs dont on a besoin
      var lien = field.lien;
      var x = field.x;
      var y = field.y;
      var type = field.type;
      var height = field.height;
      var width = field.width;
distance = distance + height



      // ajouter une image

      var imageDroite = panneauDroit.append("svg:image")
        .attr("class", "mesImage")
        .attr("id", function () {
          n = n + 1;
          return "imageDroite" + n;
        })
        .attr("xlink:href", lien) // le lien vers le chemin
        .attr("x", x)
        .attr("y", y)
        .attr("width", width)
        .attr("height", height)
        .on("click", function () {
          console.log("image clickée !");
          objetSelectione = d3.select(this); // achque foix qu'on clique sur l'image on recupere  et la valeur et on la donne objetselection 

        });
      if (type && type == "imageEtagere") {
        imageDroite.attr("class", "imageEtagere");
      }
      if (type && type == "imageCroix") {
        imageDroite.attr("class", "imageCroix");
      }
    });
  });

  //chager le cache
  // Retrieve the object from storage
  var cacheMaScene = JSON.parse(localStorage.getItem('maScene'));
  //var cacheEtagereTip = JSON.parse(localStorage.getItem('etagereTip'));

chargerCache(cacheMaScene);
}


function chargerCache(cacheMaScene){

 for (let i in cacheMaScene) {

    var cacheScene = cacheMaScene[i].listeImage;
    var cacheEtagere = cacheMaScene[i].etagere;
    heightScene = cacheMaScene[i].height;
    widthScene = cacheMaScene[i].width;
    titreSaisi = cacheMaScene[i].name;
    d3.select("#titreScene").text(titreSaisi);

    if (cacheScene) {
      console.log(JSON.stringify(cacheScene));
      for (let i in cacheScene) {
        // reinitialisation de id selon le denier element de cache, et si on ajoute un nouveau element on incrimente de 
        //1
        id = cacheScene[i].id;
        //drag image( mouvement de l'image )
        var drag = d3.drag()
          .on("drag", dragmove);
        //ajouter à la scene
        scene.push(cacheScene[i]);
        var imageCache = espace.append("svg:image")
          .attr("xlink:href", cacheScene[i].lien)
          .attr("id", cacheScene[i].id)
          .attr("class", "mesImage")
          .attr("x", cacheScene[i].x)
          .attr("y", cacheScene[i].y)
          .attr("width", cacheScene[i].width)
          .attr("height", cacheScene[i].height)
          .attr("preserveAspectRatio", "none"); // c'est pour préserver la forme de l'image 
        //.call(drag)
        imageCache.on("dblclick", function () {  // ici aussi on fait appelle a un autre evenement qui est de type double click qui fait appelle a cettte fonction qui selectionne cette element qui est l image et le suprime 
          var imageSupprime = d3.select(this)

          var idSupprime = imageSupprime.attr("id");
          imageSupprime.remove();
          for (let element in scene) {
            console.log("scene[element].id:" + scene[element].id);
            console.log(scene[element].id == idSupprime);
            if (scene[element].id == idSupprime) {
              scene.splice(element, 1);
              console.log("scene aprés suprimer:" + scene);
            }
          }
          console.log("idSupprime:" + idSupprime);
        })

        imageCache.on('contextmenu', function () {
          imageoui = true;
          return d3.contextMenu(menu, imageCache, imageoui)
        });
        imageCache.call(drag);
      }
    }
    // étagère
    if (cacheEtagere) {
      var cacheListeCroixEtagere = cacheEtagere.listeCroixEtagere;
      var cacheEtagereImage = cacheEtagere.imageEtagere;
      d3.select("#etagere").attr("height", function () { return cacheEtagereImage.height; });
      for (let i in cacheEtagere) {
        etagereTip.push(cacheEtagere[i].listeCroixEtagere);
        var url = cacheEtagereTip[i].url;
        console.log("url: " + url);
        var tipa = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function (d) {
            var v =

              'Url:'
              + '<a id=url' + cacheEtagereTip[i].id + ' href= ' + cacheEtagereTip[i].url + '>' + cacheEtagereTip[i].url + '</a>'
              + '<br/>'
            return v;
          });
        var pop = espace.append("svg:image")
          .attr("class", "croix")
          .attr("class", "mesImage")
          .attr("id", cacheEtagereTip[i].id)
          .attr("xlink:href", "img/croix.jpg")
          .attr("x", cacheEtagereTip[i].x)
          .attr("y", cacheEtagereTip[i].y)
          .attr("width", cacheEtagereTip[i].width)
          .attr("height", cacheEtagereTip[i].height)
          // c ici qu'on ajoute vraiment la tip
          .call(tipa)
        //au passage de la souris sur la croix on fait apparatre la tip
        pop.on('mouseover', tipa.show)
        //au click sur la croix la tip sera caché
        d3.select("#url" + cacheEtagereTip[i].id).text(cacheEtagereTip[i].url);

        pop.on('click', tipa.hide);
      }
    }




  }
}
 //drag image( mouvement de l'image )
    var drag = d3.drag()
      .on("drag", dragmove);


function dessinerNewObject() {
  //capter le point de localisation de lasourie là on va déposer la croix
   // Extract the click location\ 
    var point = d3.mouse(this)
      , p = { x: point[0], y: point[1] }; // recuprer le point ou j ai cliquer (x et y)

  if (objetSelectione) {
    // On n'a pas besoin d'ajouter le croix dans
    // l'espace des objets, sauf si on veut l'enregistrer
    // dans le scene.
    if (objetSelectione.attr("class") === "imageCroix") {
      return;
    }
    if (objetSelectione.attr("class") == "imageEtagere"){
      dessinerNewEtagere(p.x,p.y);
      return;
    }
   
    //ajouter un nouveau objet selectionné ds le rec

    var nouveauObjet = espace.append("svg:image")
      .attr("xlink:href", objetSelectione.attr("xlink:href"))
      .attr("id", function () {
        id = id + 1;
        return "imageSpace" + id
      })
      .attr("class", "mesImage")
      .attr("x", p.x)
      .attr("y", p.y)
      .attr("width", "50")
      .attr("height", "50")
      .attr("preserveAspectRatio", "none") // c'est pour préserver la forme de l'image 
      .call(drag)
      .on("dblclick", function () {
        // ici aussi on fait appelle a un autre evenement qui est de type double click qui fait appelle a cettte fonction qui selectionne cette element qui est l image et le suprime 
        var point = d3.mouse(this);
        var idcourant = this.attributes.id.value; 
        //suprimer de stock
 for (let i in scene.listeImage) {
          if(scene.listeImage[i].id == idcourant){
  scene.listeImage.splice(i,1);
}
        }
//supprimer du html
        d3.select(this).remove();
      })

    nouveauObjet.on('contextmenu', function () {
      var point = d3.mouse(this);

      imageoui = true;
      return d3.contextMenu(menu, nouveauObjet, imageoui)
    });
   
    // POUR AFFICHER LES LOG DANS LA CONSOLE(RELEMENT LES ATTRIBUTS DE L OBJETSELECTIONE)
    console.log("nouveauObjet///: " + nouveauObjet);
    console.log("nouveauObjet" + nouveauObjet.attr("xlink:href"));
    console.log("attr x :" + nouveauObjet.attr("x"));
    console.log("attr y:" + nouveauObjet.attr("y"));
    console.log("nouveauObjet" + nouveauObjet.attr("width"));
    console.log("nouveauObjet" + nouveauObjet.attr("height"));

    // declarer une liste d'atributs
    var listObjetAttribu = { "id": 0, "scene": "name", "type": "image", "lien": "table.jpg", "x": 510, "y": 10, "width": 50, "height": 50 };

    listObjetAttribu["id"] = nouveauObjet.attr("id");
    listObjetAttribu["scene"] = titreSaisi;
    listObjetAttribu["lien"] = nouveauObjet.attr("xlink:href");
    listObjetAttribu["x"] = parseInt(nouveauObjet.attr("x"));
    listObjetAttribu["y"] = parseInt(nouveauObjet.attr("y"));
    listObjetAttribu["width"] = parseInt(nouveauObjet.attr("width"));
    listObjetAttribu["height"] = parseInt(nouveauObjet.attr("height"));
  

//PUSH C EST FONCTION JS POUR REMPLIR UN TABLEAU
scene.listeImage.push(listObjetAttribu);
console.log("scene: " + JSON.stringify(scene));
}

  }



/**
 * 
 * @param {*} x 
 * @param {*} y 
 */
function dessinerNewEtagere(x, y){
  if (objetSelectione) {
    // On n'a pas besoin d'ajouter le croix dans
    // l'espace des objets, sauf si on veut l'enregistrer
    // dans le scene.
    if (objetSelectione.attr("class") === "imageCroix") {
      return;
    }

    // Extract the click location\ 
    //var point = d3.mouse(this)
     // ,
     //  p = { x: point[0], y: point[1] }; // recuprer le point ou j ai cliquer (x et y)

    
var nouveauSvgEtagere = espace.append("svg:g")
                              .attr("id",function(){
                                idSvgEtagere = idSvgEtagere + 1;
                                 return "idSvgEtagere"+ idSvgEtagere;
                              })
                              .on('dblclick',function(){
                                console.log("svgicicicicici");
                                this.remove();
                              })
//ajouter un nouveau objet selectionné ds le rec
    var nouveauObjet = nouveauSvgEtagere.append("svg:image")
      .attr("xlink:href", objetSelectione.attr("xlink:href"))
      .attr("id", function () {
        id = id + 1;
        return "imageSpaceEtagere" + id
      })
      .attr("class", "mesImage")
      .attr("x", x)
      .attr("y", y)
      .attr("width", "50")
      .attr("height", "50")
      .attr("preserveAspectRatio", "none") // c'est pour préserver la forme de l'image 
      //.call(drag)
      .on("dblclick", function () {
        // ici aussi on fait appelle a un autre evenement qui est de type double click qui fait appelle a cettte fonction qui selectionne cette element qui est l image et le suprime 
        var point = d3.mouse(this);

                //suprimer de stock
 for (let i in scene.listeImage) {
          if(scene.listeImage[i].id == idcourant){
  scene.listeImage.splice(i,1);
}
        }
console.log("remove image etagere");
        var elementAsupp=d3.select(this)
          .remove();
      })

    nouveauObjet.on('contextmenu', function () {
      var point = d3.mouse(this);

      imageoui = true;
      return d3.contextMenu(menu, nouveauObjet, imageoui)
    });
    //étagere<                  
    if (objetSelectione.attr("class") == "imageEtagere") {
      nouveauObjet.attr("class", "mesImage mesImageEtagere");
      //le click sur l etagère va déclancher l' ajout de tip(infobull)
      nouveauSvgEtagere.on("click", function () {
        if(!clickCroix){
        console.log("icicici");
        //capter le point de localisation de lasourie là on va déposer la croix
        var point = d3.mouse(this)

          , p = { x: point[0], y: point[1] };
          var idSvgEtagereCourant = this.attributes.id.value;
        return ajoutertip(p.x, p.y, idSvgEtagereCourant);
        }
        else{clickCroix = false;
          var result = d3.selectAll(".d3-tip").style("opacity","0");
           }
      });

    }
    // POUR AFFICHER LES LOG DANS LA CONSOLE(RELEMENT LES ATTRIBUTS DE L OBJETSELECTIONE)
    console.log("nouveauObjet///: " + nouveauObjet);
    console.log("nouveauObjet" + nouveauObjet.attr("xlink:href"));
    console.log("attr x :" + nouveauObjet.attr("x"));
    console.log("attr y:" + nouveauObjet.attr("y"));
    console.log("nouveauObjet" + nouveauObjet.attr("width"));
    console.log("nouveauObjet" + nouveauObjet.attr("height"));




    // declarer une liste d'atributs
    var listObjetAttribu = { "id": 0, "scene": "name", "type": "image", "lien": "table.jpg", "x": 510, "y": 10, "width": 50, "height": 50 };

    listObjetAttribu["id"] = nouveauObjet.attr("id");
    listObjetAttribu["scene"] = titreSaisi;
    listObjetAttribu["lien"] = nouveauObjet.attr("xlink:href");
    listObjetAttribu["x"] = parseInt(nouveauObjet.attr("x"));
    listObjetAttribu["y"] = parseInt(nouveauObjet.attr("y"));
    listObjetAttribu["width"] = parseInt(nouveauObjet.attr("width"));
    listObjetAttribu["height"] = parseInt(nouveauObjet.attr("height"));



    
if(objetSelectione.attr("class") == "imageEtagere"){
  listObjetAttribu["id"] = nouveauSvgEtagere.attr("id");
var monEtagere = {
  "imageEtagere":"",
  "listeCroixEtagere": []
}
monEtagere.imageEtagere = listObjetAttribu;
//listeEtagere.push(monEtagere);
scene.listeEtagere.push(monEtagere);
}else{
//PUSH C EST FONCTION JS POUR REMPLIR UN TABLEAU
scene.listeImage.push(listObjetAttribu);
console.log("scene: " + JSON.stringify(scene));
}

  }


}




// deplacer l image
function dragmove(d) {
  var x = d3.event.x;
  var y = d3.event.y;
  console.log("ici" + "x:" + x + "y" + y);
  var image = d3.select(this)
    .attr("x", x)
    .attr("y", y);
  var idImageDeplace = image.attr("id");
  for (let element in scene) {
    console.log("id element à deplacer scene[element].id:" + scene[element].id);
    console.log(scene[element].id == idImageDeplace);
    if (scene[element].id == idImageDeplace) {
      scene[element].x = x;
      scene[element].y = y;
      console.log("scene aprés deplacer:" + scene);
    }
  }
}

// foction infobulle
function ajoutertip(x, y, idSvgEtagereCourant) {
  //on a mis une condition pour permettre de déposer juste les images dont le nom est croix_lien
  if (objetSelectione && objetSelectione.attr("class") === "imageCroix") {
    var url;
    //var idCroix = 0;
    idUrl = idUrl + 1;
    idLien = idLien + 1;
    console.log("idurl: " + idUrl);
    //tip = d3.tip()
     var croixEtagereSvg= d3.select("#"+idSvgEtagereCourant);

      var maTipCroixGroup = croixEtagereSvg.append("g")
var divScene= d3.select("#scene");
      var tip = divScene.append("div")
                    .attr('class', 'd3-tip tip-input')
                    .attr('id',function(){
                       return "id-Tip"+ idUrl;
                     })
                    .html("<p>ccccc</p>")
                    .style("opacity", .9)
                    .style("left", (d3.event.pageX + 30) + "px")     
                .style("top", (d3.event.pageY - 30) + "px");

      //d3.select(".tooltip").style("opacity",.10);
      //.style("opacity",.9);
      //.offset([-10, 0])
      //instancie l'objet tip puis le rempli nous on a mis dedans les label url,saisir un lien et botton enregi 
      tip.html(function (d) {
        var v =
          'Url:'
          + '<a id=url' + idUrl + '>' + '</a>'
          + '<br/>'
          + '<div class="divForm">'
          // +'<form>'
          + '<label for="myLien"> Saisir un lien:</label>'
          //au click sur la croix la tip sera caché
          //cette fonction grace a d3 elle capte le id de cette input #myLien  at caque modif dedans elle recupere la valeur saisie ligne 325
          + '<input type="text" id="myLien' + idLien + '" name="myLien" onClick= "saisirUrl()" ><br>'
          //le click sur le boutton enregistrer va executer la fonction saveUrl()
          + '<input value="Enregistrer" type="button" onClick= "saveUrl('+idSvgEtagereCourant+ ','+x +','+ y+')"><br>'
          // + '<input type="submit" value="save"/>'
          //  +'</form>'
          + '</div>\
	   <style>\
	   .divForm{\
		padding: 4px;\
	   }\
	   .divForm input{\
		margin-top: 4px;\
	   }\
	   </style>';
        return v;
      });



    //capter le point de localisation de lasourie là on va déposer la croix
    /*var point = d3.mouse(this)
    , p = {x: point[0], y: point[1] };*/

    //xx= p.x;
    //yy=p.y;
    xx = x;
    yy = y;
    //le dessin de la croix 
   // var croixEtagere = espace.append("svg")
   //var croixEtagereSvg = d3.select("#"+idSvgEtagereCourant);

   var croixEtagere=maTipCroixGroup.append("svg:image")
      .attr("class", "croix")
      .attr("class", "mesImage")
      .attr("id", function () {
        idCroix = idCroix + 1;
        return "croix" + idCroix;
      })
      .attr("xlink:href", objetSelectione.attr("xlink:href"))
      .attr("x", xx)
      .attr("y", yy)
      .attr("width", "20")
      .attr("height", "20");
      
      // c ici qu'on ajoute vraiment la tip
      //.call(tip);

    //au passage de la souris sur la croix on fait apparatre la tip
    croixEtagere.on('mouseover', function(){
      d3.select(".d3-tip").style("opacity",.9);
    });
//var ici = d3.select(this);
   //   return tip.show;});




    //au click sur la croix la tip sera caché
   
    croixEtagere.on('click', function(){

              clickCroix = true;
              d3.selectAll(".d3-tip").attr("style","opacity : 0");
              //var result =tip.hide();
               //  return result;
                });
    // supprimer une croix
    croixEtagere.on("dblclick", function () {  // ici aussi on fait appelle a un autre evenement qui est de type double click qui fait appelle a cettte fonction qui selectionne cette element qui est l image et le suprime 
      d3.select(this)
        .remove();
    })


  }
}
 

// saisir l url dans le input et afficher le lien en haut sous forme de lien clicable 
function saisirUrl() {
  console.log("récuperer les valeur des input..");
  console.log("************idUrl" + idUrl);
  //when the input lien sur la croix
  d3.select("#myLien" + idLien).on("input", function () {
    console.log("on est ds input tip");
    console.log("la lien saisi: " + this.value)
    urlSaisi = this.value;

    //apres cette la copie dans url: lien (en haut de input) sous form de lien clickable 

    d3.select("#url" + idUrl).text(urlSaisi);

    d3.select("#url" + idUrl).property("href", urlSaisi);



  });

}

function saveUrl(idSvgEtagereCourant,x,y) {
  //stockage ds localstorage



  var valeurUrl = d3.select("#url" + idUrl).property("href");
var tip = d3.select("#scene").append("div")
                    .attr('class', 'd3-tip n')
                    .attr('id',function(){
                       return "id-Tipfinal"+ idUrl;
                     })
                    .html("<p>ccccc</p>")
                    .style("opacity", .9)
                    .style("position",'absolue')
                   .style("left",x + "px")     
                .style("top", y + "px");


   tip.html(function (d) {
    var v =
      'Url:'
      + '<a id=url' + idUrl + ' href= ' + valeurUrl + ' >' + valeurUrl + '</a>';
    return v;
  });
  // declarer une liste d'atributs
  var listTipAtribut = { "id": "0", "type": "image", "lien": "", "url": "https://www.google.fr/", "x": "510", "y": "10", "width": "", "height": "" }; // initialisation 
  listTipAtribut["url"] = urlSaisi;
  listTipAtribut["x"] = xx;
  listTipAtribut["y"] = yy;

  if (urlSaisi) {
    //PUSH C EST FONCTION JS POUR REMPLIR UN TABLEAU
    listTipAtribut["id"] = idUrl;
    var lien = d3.select("#croix" + idUrl).attr("href");
    listTipAtribut["lien"] = lien;
    listTipAtribut["width"] = d3.select("#croix" + idUrl).attr("width");
    listTipAtribut["height"] = d3.select("#croix" + idUrl).attr("height");
    scene.listeEtagere.forEach(function(d){
if(d.imageEtagere.id == idSvgEtagereCourant.attributes.id.value){
  d.listeCroixEtagere.push(listTipAtribut);
};
    } );
    
    console.log("etagereTip: " + JSON.stringify(scene));
   
  }
  // fermer le tip
 // var tipAsupprimer=d3.selectAll(".d3-tip").style("opacity",0);
  d3.selectAll(".tip-input").remove();
  //tipAsupprimer = this.remove();
}






// menu contextuel

function resize(d, i, e, o, v) {
	/**********************
	 d = Le slider qu'on changé
	 i = index
	 e = 
	 o = l'objet ou l'image qu'on vient de cliquer
	 v = la valeur du slider
	
	
	***************************/
  var idResize = o.attr("id");
  var h = o.attr("height");
  var w = o.attr("width");
  // REDIMENTIONNER L IMAGE AVEC LE MENU CONTEXTUEL
  switch (d.idRange) {
    case "rangeH":
      if (idResize == "scene") {

        var taux = h / v;
        var newValueH = h / taux
        // o.attr("height", nouvelle); à vérifier pourquoi ça ne fonctionne pas 
        d3.select('#scene').attr("style", "height:" + newValueH + "px");
        heightScene = newValueH;
        o.attr("height", newValueH);
        // d3.select('#scene').attr("style","height:"+newValueH+"px");
        var touteImages = d3.selectAll(".mesImage").nodes().map(function (d) { return d.attributes.x.value; })
        console.log("touteImages:" + touteImages);
        var panelDroiteImages = d3.selectAll(".mesImage")
          .nodes()
          .map(function (d) {
            //y
            var oldY = d.attributes.y.value;
            var id = d.attributes.id.value
            d3.selectAll(".mesImage" + "#" + id).attr("y", oldY / taux);
            var newY = d.attributes.y.value;
            //h
            var oldH = d.attributes.height.value;
            var id = d.attributes.id.value
            d3.selectAll(".mesImage" + "#" + id).attr("height", oldH / taux);
            var newH = d.attributes.height.value;
            //
            return newH;
          });
        /*var xx= images.forEach(function(ici){
         console.log("ici = "+ici["attributes"].x);
         var k = ici["attributes"].x.codeValue;
          return ici;
        })*/


        console.log(panelDroiteImages);
        // var size = images.size();
      } else {
        o.attr("height", v);
        for (let element in scene) {
          console.log("id element a resizer scene[element].id:" + scene[element].id);
          console.log(scene[element].id == idResize);
          if (scene[element].id == idResize) {
            scene[element].height = v;
            console.log("scene aprés resizer:" + scene);
          }
        }
      }



      break;
    case "rangeW":

      if (idResize == "scene") {

        var tauxW = w / v;
        var newValueW = w / tauxW;
        //d3.select('#scene').attr("width",newValueW); à vérifier aussi
        d3.select('#scene').attr("style", "width:" + newValueW);

        widthScene = newValueW;
        o.attr("width", widthScene);
        var panelDroiteImages = d3.selectAll(".mesImage")
          .nodes()
          .map(function (d) {
            //y
            var oldX = d.attributes.x.value;
            var id = d.attributes.id.value
            d3.selectAll(".mesImage" + "#" + id).attr("x", oldX / tauxW);
            var newX = d.attributes.x.value;
            //W
            var oldW = d.attributes.width.value;
            var id = d.attributes.id.value
            console.log(oldW / tauxW);
            d3.selectAll(".mesImage" + "#" + id).attr("width", oldW / tauxW);
            var newW = d.attributes.width.value;

            //
            return newW;
          });
        console.log(panelDroiteImages);
      } else {
        o.attr("width", v);
        for (let element in scene) {
          if (scene[element].id == idResize) {
            scene[element].width = v;
          }
        }
      }
      break;
    default:
      break;
  }
}



var postDate= {};
  
    


function enregistrement() {

  console.log("SAVE DANS localstorage....");

  localStorage.setItem('scene', JSON.stringify(scene));
  //localStorage.setItem('etagereTip', JSON.stringify(etagereTip));
  
  /*for (let objet of scene) {
    for (let p in objet) {

      console.log(p + " : " + objet[p]);

    }

  }*/

console.log("SAVE SUR LE SERVER....");

  //service
var url = "localhost:8888/envoi/test"
  d3.request(url)
   .header("X-Requested-With", "XMLHttpRequest")
   .header("Content-Type", "application/json")
   .post("{scene}", function(err,rows){
    console.log("err"+ err);
   });

}
