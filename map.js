   var sceneAffichee = true;
            var marker = new Array();
            var currentPoint;
 
var listeEtagere;
      function initialisation() {
        var markerCompusTriolet;
        var groupeLayer;
        var dataLatLng = [
          {"name":"compusTriolet","lat":43.631522, "lng":3.863455},
         {"name":"valery","lat":43.632611, "lng":3.870452},
         {"name":"valery","lat":43.633700, "lng":3.872500},
         {"name":"valery","lat":43.638400, "lng":3.875400}
          ];

          var rect;
var rectExist= false;
var rectImageLayer;
        
          var mymap = L.map('mapid').setView([43.631522, 3.863455], 13);// carte map et 13 cest lezom
           L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
           attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
           maxZoom: 18,
           id: 'mapbox.streets',
           accessToken: 'pk.eyJ1IjoibnNhYWRhIiwiYSI6ImNqcmJ0eXZxZjAwamQ0OXBheHd3Z2c2cHgifQ.h8Y6rCLGY27RC9Qbg4mxUQ'
                }).addTo(mymap);
// pour chaque scene
for (var i in dataLatLng) {
  var scene ;
   var height;
  var listeImage =[];
  var listeEtagere=[];
  var listeEtagere=[
      {
        "imageEtagere": {},
        "listeCroixEtagere": []
      }]
  var listeCroixEtagere = [];
  var lat = dataLatLng[i].lat;
  var lng = dataLatLng[i].lng;
  var width =510; // La longuer de rectangle
var height =505; // La largeur de rectangle
  groupeLayer = new L.layerGroup();
 
  marker = L.marker([lat,lng]).bindPopup(dataLatLng[i].name);
  groupeLayer.addLayer(marker);
 groupeLayer.addTo(mymap);

 marker.on("click",function(e){
  console.log(e.target.getLatLng());
  var zoom = mymap.getZoom();
  console.log(zoom);
   if(rect){
    rectImageLayer.remove();
   }
///
var latLng = e.target.getLatLng();//pour représenter un point geographiquement

// chercher les pixel de ce point 
currentPoint = mymap.latLngToContainerPoint(latLng);
//définir d'autre variable



   ///
   $.getJSON('emplacement.json', function(data){//data c'est la liste
   //var data=JSON.parse(localStorage.getItem('maScene'));
   for (let objet of data) { 
    scene = data[0].name;
    height =data[0].height;
    width = data[0].height;
   listeImage = data[0].listeImage;
   listeEtagere = data[0].listeEtagere

   //imageEtagere = data[0].etagere.imageEtagere;
   //listeCroixEtagere = data[0].etagere.listeCroixEtagere;
         //j'ai rajoute 200 en haut a gauche
var southWest = L.point(currentPoint.x,(currentPoint.y+height));//pour positioner le marqueur sur le point supérieur gauche
var northEast=L.point((currentPoint.x +width),currentPoint.y);
var bounds2= L.latLngBounds(mymap.containerPointToLatLng(northEast),mymap.containerPointToLatLng(southWest));//définit les limites géographiques d'un rectangle
rect = L.rectangle(bounds2, {color: "white", background:"white", weight:4 })


rectImageLayer = new L.featureGroup(); // un nouveau layer qui va contenir le rectangle et images
rectImageLayer.addLayer(rect);
if(mymap.getZoom()>= 14){
  groupeLayer.addLayer(rectImageLayer);
}
mymap.on("zoom",function(){
  console.log("zoom:"+mymap.getZoom());
  if(mymap.getZoom()>= 14){
    groupeLayer.addLayer(rectImageLayer);
    //center la map sur cet element
//mymap.fitBounds(bounds2);
  }else{
    rectImageLayer.remove();
  }
}

);

  for (let objet of listeImage) { 
         dessiner(objet, mymap, rectImageLayer,currentPoint,i)
       }
       //etagère
        // dessiner(imageEtagere, mymap,rectImageLayer, currentPoint,i);
 for (let etagere of listeEtagere) { 
        var groupeEtagereCroixLayer = new L.layerGroup();
        var etagereImage =etagere.imageEtagere;
         dessiner(etagereImage, mymap, rectImageLayer,currentPoint,i,groupeEtagereCroixLayer);
        //croix
          for (let croix of etagere.listeCroixEtagere) { 
         dessiner(croix, mymap,rectImageLayer,currentPoint,i);
       }
       
       }
        
      }
      });
   

 
})

marker.on("dblclick",function(){
  
  rectImageLayer.remove();
})

}

      }
 /**
  * 
  * @param {*} L 
  * @param {*} mymap 
  * @param {*} currentPoint 
  */         
function chargerScene(L, mymap,rectImageLayer, currentPoint,i) {
  // console.log(sceneAffichee)
  // console.log(marker)

   $.getJSON('emplacement.json', function(data){//data c'est la liste
   //var data=JSON.parse(localStorage.getItem('maScene'));
   for (let objet of data) { 
   var scene = data[0].name;
   var height =data[0].height;
  var listeImage = data[0].listeImage;
  var listeEtagere = [0].listeEtagere
  var imageEtagere = data[0].etagere.imageEtagere;
  var listeCroixEtagere = data[0].etagere.listeCroixEtagere;
         if(sceneAffichee==false){
          sceneAffichee = true ; 
           }else{
            sceneAffichee = false ; 
                }
  for (let objet of listeImage) { 
         dessiner(objet, mymap, rectImageLayer,currentPoint,i)
       }
       //etagère
         dessiner(imageEtagere, mymap,rectImageLayer, currentPoint,i);
          for (let objet of listeCroixEtagere) { 
         dessiner(objet, mymap,rectImageLayer,currentPoint,i);
       }
      }
      });
    }

      
function dessiner(objet,mymap,rectImageLayer, currentPoint,i, groupeEtagereCroixLayer){
  imageLayer = new L.layerGroup(); 
  var x = objet.x; 
                   let y = objet.y;
                   var height = objet.height;
                   var width  = objet.width;
                   let image = objet.lien;
                   let url = objet.url;
                   console.log("image="+image)
                   // let width = objet.width;
                   // let height = objet.height; 
                   //icone image 
                   let myIcon = L.icon({
                    iconUrl: image,
                    iconSize: [width, height],   
                });
                //la localisation de l image en dur à 30px / au rectangle
               var imageX= currentPoint.x+ x;
               var imageY= currentPoint.y+ y;
                console.log("iciciciciic: currentPoint.x+x: "+ imageX); 

                   let localImage = L.point(imageX,imageY); // pour localiser l'endroit de dépot de l'image par rapport au rectangle 
                // les atrributs de mon icone(image)
                var imarker = new L.marker(mymap.containerPointToLatLng(localImage), {icon: myIcon});

  //popUp
                if(url){
                
                var popupContent = "<a >"+url+"</a>" 
     
    imarker.bindPopup(popupContent).openPopup();
  }

imageLayer.addLayer(imarker);
rectImageLayer.addLayer(imageLayer);
//rectImageLayer.addTo(mymap);
                  /* if(sceneAffichee==false){              
                   var imarker = new L.marker(mymap.containerPointToLatLng(localImage), {icon: myIcon});
                  mymap.addLayer(imarker);
                  marker.push(imarker);// ajoute un ou plusieurs éléments à la fin d'un tableau et retourne la nouvelle taille du tableau
                  console.log(sceneAffichee);
                  }else{
                  for(i=0;i<marker.length;i++) {
                    mymap.removeLayer(marker[i]);
                  }               
                  //console.log(sceneAffichee);
                  }*/
               
               // L.marker(mymap.containerPointToLatLng(localImage), {icon: myIcon}).addTo(mymap); // ajouter l'icone (l'image) sous forme d'un marker a ma map  
                  }
   

    

         
